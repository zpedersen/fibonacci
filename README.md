#Zach Pedersen

My entry for CST-307 Procedures and Stack Management.

Installation Steps:

1. Download and install QTSPIM.
2. Download and extract Fibonacci.asm program from BitBucket repository.
3. Launch QTSPIM and left click on Load File at the top left.
4. Select Fibonacci program and run. 
5. Input desired number and receive result.